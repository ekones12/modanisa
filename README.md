

# Modanisa Technologist Interview Case Study

This case study involves the implementation of a todos API(NodeJs) and implementation of todos Client(VueJs). 
There is ONLY adding todos functionality as requested in the technical documentation,

This application was developed using technologies in below:

   * **Vue.js**
   
   * **Express.js**
   
   * **Node.js**
   
   *  **MongoDb**
### Prerequisites

Docker and docker-compose used.

`sudo apt-get update`

`sudo apt-get install docker && docker-compose`

### Running

`docker-compose up -d --build`

App running defult port:
> http://localhost:8080

Change ENV variables from docker-compose to deploy different ports and hosts

```
 backend:
    container_name: backend
    build:
      context: ./backend
    depends_on:
      - mongo
    volumes:
      - ./backend:/app_backend
      - /app_backend/node_modules
    environment:
      - MONGO_URL=mongodb://mongo:27017/modanisa
      - APP_PORT=3000
    ports: [ '3000:3000' ]
  mongo:
    container_name: mongo
    image: mongo:4.0
    ports:
      - "27017:27017"
    restart: always
  frontend:
    container_name: frontend
    build:
      context: ./frontend
    volumes:
      - ./frontend:/app_frontend
      - /app_frontend/node_modules
    ports:
      - '8080:8080'
    environment:
      - BACKEND_URL=http://127.0.0.1:3000/
 
``` 

### Running all the tests

Backend:
    
    cd backend
    NODE_ENV=test mocha --exit

Frontend:

    cd frontend
    vue-cli-service test:unit
    vue-cli-service test:e2e

### LIVE DEMO 

[Vue JS Live Demo] https://modanisa-vue.eneskocak.com/

[Backend API] https://modanisa-api.eneskocak.com/

