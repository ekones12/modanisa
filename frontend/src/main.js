import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';
import App from './App.vue';
import VueAxios from 'vue-axios'


Vue.use(BootstrapVue);
Vue.use(VueAxios, axios)

Vue.axios.defaults.baseURL =  process.env.BACKEND_URL ? process.env.BACKEND_URL : 'http://localhost:3000/';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
