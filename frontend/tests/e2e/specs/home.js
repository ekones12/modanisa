// https://docs.cypress.io/api/introduction/api.html

describe('Home', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Should display the todo list.', () => {
    cy.get('.todoList').should('be.visible');
  });

  it('Should display the create todo input.', () => {
    cy.get('input[id="newTodoLabel"]').should('be.visible');
  });

  it('Should display the create todo button.', () => {
    cy.get('button[id="createTodo"]').should('be.visible');
  });

});
