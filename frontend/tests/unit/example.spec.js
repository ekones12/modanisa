import {shallowMount} from '@vue/test-utils'
import CreateTodo from "@/components/CreateTodo";
import axios from "axios";

jest.mock("axios", () => ({
	post: jest.fn(),
	get: jest.fn()
}))

describe('CreateTodo.vue', () => {

	it('adding todo on create button triggered', async () => {
		const wrapper =shallowMount(CreateTodo, {
			propsData: {
				label:"test label"
			}
		})

		await wrapper.find('#createTodo').trigger('click')

		expect(axios.post).toHaveBeenCalledTimes(1)
		expect(axios.post).toHaveBeenCalledWith('/api/todos',{label: "test label"})
	})



	it('cleat input label after todo created', async () => {
		const wrapper = shallowMount(CreateTodo, {
			propsData: {
				label:"test label"
			}
		})

		await wrapper.find('#createTodo').trigger('click')

		expect(wrapper.vm.propVal).toBe('')

	})
})


