const Todo = require('../models/Todo');

class TodoRepository {
	/**
	 * @param {*} model
	 */
	constructor(model) {
		this.model = model;
	}

	create(label) {
		const newTodo = { label };
		const todo = new this.model(newTodo);
		return todo.save();
	}

	findAll() {
		return this.model.find();
	}
}

module.exports = new TodoRepository(Todo);
