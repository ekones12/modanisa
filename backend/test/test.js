const request = require('supertest');
const expect = require('chai').expect;
const app = require('../app');
const {connectDB, disconnectDB} = require('../config/database');

describe('POST /api/todos', () => {
	before(async () => {
		await connectDB();
	});

	it('Add new todo', async () => {
		const res = await request(app)
			.post('/api/todos')
			.send({label: 'Test ToDo Post'});
		expect(res.body).to.contain.property('label');

	});

	it('Returns 400 status code if required field is null', async () => {

		const res = await request(app).post('/api/todos').send({});

		expect(res.statusCode).to.eq(400);


	});
});


describe('GET /api/todos', () => {
	before(async () => {
		await connectDB();
	});

	after(async () => {
		await disconnectDB();
	});

	it('fetch todos', async () => {
		const res = await request(app)
			.get('/api/todos')
		expect(res.body).to.contain.property('todos');

	});

	it('Returns 400 status code if endpoint not correct', async () => {

		const res = await request(app).get('/api/todoss')
		expect(res.statusCode).to.eq(404);

	});
});
