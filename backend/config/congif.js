module.exports = {
	DB: process.env.MONGO_URL ? process.env.MONGO_URL : 'mongodb://127.0.0.1:27017/modanisa',
	APP_PORT: process.env.APP_PORT ? process.env.APP_PORT : 3000,
};
