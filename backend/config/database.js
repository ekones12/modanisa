const mongoose = require('mongoose');
const config = require('./congif');
const { MongoMemoryServer } = require('mongodb-memory-server');

const mongoURI = config.DB;

const options = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false
};

let mockDb = new MongoMemoryServer();

const connectDB = async () => {
	// Mocking Database if environment is 'test'
	if (process.env.NODE_ENV === 'test') {
		try {
			const mockURI = await mockDb.getUri();

			await mongoose.connect(mockURI, options);
		} catch (err) {
			console.error(err.message);
		}
	} else {
		try {
			await mongoose.connect(mongoURI, options);

			console.log('MongoDb Connected.');
		} catch (err) {
			console.error(err.message);

			process.exit(1);
		}
	}
};

const disconnectDB = async () => {
	try {
		await mongoose.disconnect();
	} catch (err) {
		console.error(err.message);
	}
};

module.exports = { connectDB, disconnectDB };
