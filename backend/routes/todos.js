var express = require('express');
var router = express.Router();
const todoRepository = require('../repository/TodoRepository');


/* GET users listing. */
router.get('/', function (req, res, next) {
	todoRepository.findAll()
		.then((todos) => {
			res.json({todos: todos});
		})
		.catch((error) => {
			console.log(error)
			res.status(400).json({status: 400, message: error})
		});

});

/* GET users listing. */
router.post('/', function (req, res, next) {
	const label = req.body.label;
	if(!label)
		res.status(400).json({status: 400, message: "Required Field is null"})
	else
		todoRepository.create(label)
			.then((todo) => {
				res.json({label: todo.label});
			})
			.catch((error) => {
				console.log(error)
				res.status(400).json({status: 400, message: error})
			});
});

module.exports = router;
