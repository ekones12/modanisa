const mongoose = require('mongoose');

const { Schema } = mongoose;

// Define schema for todo items
const todoSchema = new Schema({
	label: {
		type: String,
	},
});

const Todo = mongoose.model('Todo', todoSchema);

module.exports = Todo;
